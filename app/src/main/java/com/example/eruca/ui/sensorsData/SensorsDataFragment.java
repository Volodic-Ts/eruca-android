package com.example.eruca.ui.sensorsData;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.room.Room;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eruca.BuildConfig;
import com.example.eruca.R;
import com.example.eruca.boxes.Box;
import com.example.eruca.boxes.BoxViewModel;
import com.example.eruca.room.AppExecutors;
import com.example.eruca.room.DatabaseController;
import com.example.eruca.room.database.UserDatabase;
import com.example.eruca.room.entity.UserTable;
import com.example.eruca.ui.sensorsData.manualControl.AirHumidityFragment;
import com.example.eruca.ui.sensorsData.manualControl.AirTemperatureFragment;
import com.example.eruca.ui.sensorsData.manualControl.BrightnessFragment;
import com.example.eruca.ui.sensorsData.manualControl.SoilMoistureFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@SuppressLint("UseSwitchCompatOrMaterialCode")
public class SensorsDataFragment extends Fragment {

    private RequestQueue mQueue;

    private TextView mSubHeaderView;

    private TextView mAirTemperatureText;
    private TextView mAirHumidityText;
    private TextView mFluidLevelText;
    private TextView mBrightnessText;
    private TextView mSoilMoistureText;

    private Switch mTemperatureSwitch;
    private Switch mHumiditySwitch;
    private Switch mBrightnessSwitch;
    private Switch mSoilMoistureSwitch;

    private ImageView mTemperatureManualControl;
    private ImageView mHumidityManualControl;
    private ImageView mBrightnessManualControl;
    private ImageView mSoilMoistureManualControl;

    private String TOKEN = "";
    private String BOX_ID = "";
    private String BOX_TITLE = "";

    private ConstraintLayout layout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TOKEN = requireActivity().getIntent().getStringExtra("token");

        BoxViewModel viewModel = new ViewModelProvider(requireActivity()).get(BoxViewModel.class);

        Observer<Box> observer = box -> {
            SharedPreferences mPreferences = getActivity().getSharedPreferences("boxStateData", Context.MODE_PRIVATE);
            SharedPreferences.Editor mEditor = mPreferences.edit();

            BOX_ID = box.getBoxId();
            BOX_TITLE = box.getBoxTitle();

            mEditor.putString("id", BOX_ID);
            mEditor.putString("title", BOX_TITLE);
            mEditor.apply();

            parseSensorsDataFromServer(TOKEN, BOX_ID);

            mSubHeaderView.setText(getString(R.string.sensors_data_sub_header, BOX_TITLE));
        };

        viewModel.getData().observe(this, observer);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sensors_data_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mQueue = Volley.newRequestQueue(getContext());

        layout = requireActivity().findViewById(R.id.container);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            layout.getForeground().setAlpha(0);
        }

//        Log.d("Login: ", controller.getSavedUser().toString());

        mSubHeaderView = view.findViewById(R.id.sensors_data_sub_header);

        mAirTemperatureText = view.findViewById(R.id.air_temperature_value);
        mAirHumidityText = view.findViewById(R.id.humidity_value);
        mBrightnessText = view.findViewById(R.id.brightness_value);
        mFluidLevelText = view.findViewById(R.id.fluid_level_value);
        mSoilMoistureText = view.findViewById(R.id.soil_moisture_value);

        mBrightnessSwitch = view.findViewById(R.id.brightness_switch);
        mTemperatureSwitch = view.findViewById(R.id.air_temperature_switch);
        mHumiditySwitch = view.findViewById(R.id.humidity_switch);
        mSoilMoistureSwitch = view.findViewById(R.id.soil_moisture_switch);

        mTemperatureManualControl = view.findViewById(R.id.manual_control_air_temp);
        mHumidityManualControl = view.findViewById(R.id.manual_control_humidity);
        mBrightnessManualControl = view.findViewById(R.id.manual_control_brightness);
        mSoilMoistureManualControl = view.findViewById(R.id.manual_control_soil_moisture);

        mSubHeaderView.setText(getString(R.string.sensors_data_sub_header, getBoxState("title")));

        parseSensorsDataFromServer(TOKEN, getBoxState("id"));

        mBrightnessSwitch.setChecked(getSwitchesState("brightness"));
        mTemperatureSwitch.setChecked(getSwitchesState("temperature"));
        mHumiditySwitch.setChecked(getSwitchesState("humidity"));
        mSoilMoistureSwitch.setChecked(getSwitchesState("soil_moisture"));

        setImagesState(mBrightnessSwitch, mBrightnessManualControl);
        setImagesState(mTemperatureSwitch, mTemperatureManualControl);
        setImagesState(mHumiditySwitch, mHumidityManualControl);
        setImagesState(mSoilMoistureSwitch, mSoilMoistureManualControl);

        mTemperatureSwitch.setOnCheckedChangeListener(new CustomSwitchListener(mTemperatureSwitch, mTemperatureManualControl, "temperature"));
        mHumiditySwitch.setOnCheckedChangeListener(new CustomSwitchListener(mHumiditySwitch, mHumidityManualControl, "humidity"));
        mBrightnessSwitch.setOnCheckedChangeListener(new CustomSwitchListener(mBrightnessSwitch, mBrightnessManualControl, "brightness"));
        mSoilMoistureSwitch.setOnCheckedChangeListener(new CustomSwitchListener(mSoilMoistureSwitch, mSoilMoistureManualControl, "soil_moisture"));

        mTemperatureManualControl.setOnClickListener(new ManualControlClickListener(mTemperatureManualControl, new AirTemperatureFragment(), "temperature"));
        mHumidityManualControl.setOnClickListener(new ManualControlClickListener(mHumidityManualControl, new AirHumidityFragment(), "humidity"));
        mBrightnessManualControl.setOnClickListener(new ManualControlClickListener(mBrightnessManualControl, new BrightnessFragment(), "brightness"));
        mSoilMoistureManualControl.setOnClickListener(new ManualControlClickListener(mSoilMoistureManualControl, new SoilMoistureFragment(), "soil_moisture"));
    }

    private void parseSensorsDataFromServer(String token, String boxId) {
        String url = BuildConfig.SERVER_URL + "/sensors/get/" + boxId;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, response -> {
            try {
                JSONObject sensorsData = response.getJSONObject("sensorsData");

                String temperature = String.valueOf(sensorsData.getDouble("temperature"));
                String humidity = String.valueOf(sensorsData.getDouble("humidity"));
                String brightness = String.valueOf(sensorsData.getInt("brightness"));
                String fluidLevel = String.valueOf(sensorsData.getInt("fluidLevel"));
                String soilMoisture = String.valueOf(sensorsData.getInt("soilMoisture"));

                mAirTemperatureText.setText(getString(R.string.air_temp_value, temperature));
                mAirHumidityText.setText(getString(R.string.air_humidity_value, humidity));
                mBrightnessText.setText(getString(R.string.brightness_value, brightness));
                mFluidLevelText.setText(getString(R.string.fluid_level_value, fluidLevel));
                mSoilMoistureText.setText(getString(R.string.soil_moisture_value, soilMoisture));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, Throwable::printStackTrace) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + token);

                return headerMap;
            }
        };

        mQueue.add(request);
    }

    private void saveSwitchStates(String tag, Switch mSwitch) {
        SharedPreferences mPreferences = requireActivity().getSharedPreferences("sensors_data_state", MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mPreferences.edit();

        mEditor.putBoolean(tag, mSwitch.isChecked());
        mEditor.apply();
    }

    private boolean getSwitchesState(String tag) {
        SharedPreferences mPreferences = requireActivity().getSharedPreferences("sensors_data_state", MODE_PRIVATE);

        boolean flag = false;
        return mPreferences.getBoolean(tag, flag);
    }

    private void setImagesState(Switch mSwitch, ImageView mImageView) {
        if (mSwitch.isChecked()) {
            mImageView.setVisibility(View.VISIBLE);
        } else {
            mImageView.setVisibility(View.GONE);
        }
    }

    private String getBoxState(String tag) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("boxStateData", Context.MODE_PRIVATE);

        return sharedPreferences.getString(tag, "");
    }

    private void onSwitchChangeStatePopupWindow(View view, Switch mSwitch, ImageView mImageView) {

        LayoutInflater inflater = (LayoutInflater) requireActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.manual_control_popup_fragment, null);

        Button acceptButton = popupView.findViewById(R.id.accept_button);
        Button denyButton = popupView.findViewById(R.id.deny_button);

        int width = ConstraintLayout.LayoutParams.WRAP_CONTENT;
        int height = ConstraintLayout.LayoutParams.WRAP_CONTENT;

        PopupWindow window = new PopupWindow(popupView, width, height, false);
        window.showAtLocation(view, Gravity.CENTER, 0, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            layout.getForeground().setAlpha(128);
        }

        acceptButton.setOnClickListener(view1 -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                layout.getForeground().setAlpha(0);
            }
            mImageView.setVisibility(View.VISIBLE);
            mSwitch.setChecked(true);
            window.dismiss();
        });

        denyButton.setOnClickListener(view1 -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                layout.getForeground().setAlpha(0);
            }
            mImageView.setVisibility(View.GONE);
            mSwitch.setChecked(false);
            window.dismiss();
        });

        window.setOnDismissListener(() -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                layout.getForeground().setAlpha(0);
            }
        });
    }

    private class CustomSwitchListener implements CompoundButton.OnCheckedChangeListener {
        private Switch mSwitch;
        private ImageView mImageView;
        private String mTag;

        public CustomSwitchListener(Switch mSwitch, ImageView mImageView, String mTag) {
            this.mSwitch = mSwitch;
            this.mImageView = mImageView;
            this.mTag = mTag;
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            saveSwitchStates(mTag, mSwitch);
            if (b) {
                onSwitchChangeStatePopupWindow(getView(), mSwitch, mImageView);
            } else {
                mImageView.setVisibility(View.GONE);
            }
        }
    }

    private class ManualControlClickListener implements View.OnClickListener {
        private ImageView mImageView;
        private Fragment mFragment;
        private String mTag;

        public ManualControlClickListener(ImageView mImageView, Fragment mFragment, String mTag) {
            this.mImageView = mImageView;
            this.mFragment = mFragment;
            this.mTag = mTag;
        }

        @Override
        public void onClick(View view) {
            requireActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.main_container, mFragment)
                    .addToBackStack(mTag)
                    .commit();
        }
    }
}