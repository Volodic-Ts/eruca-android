package com.example.eruca.ui.techCard;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eruca.BuildConfig;
import com.example.eruca.R;
import com.example.eruca.heplers.CustomJsonObjectRequest;
import com.example.eruca.heplers.model.Template;
import com.example.eruca.ui.techCard.template.TemplateFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.slider.RangeSlider;
import com.google.android.material.slider.Slider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TechCardFragment extends Fragment {

    private RequestQueue mQueue;

    private TextView mSubHeaderText;

    private EditText mDayBeginInput;
    private EditText mDayEndInput;

    private Slider mAirHumiditySlider;
    private Slider mFluidLevelSlider;

    private RangeSlider mAirTemperatureSlider;
    private RangeSlider mSoilMoistureSlider;

    private Button mSendButton;
    private Button mDeleteButton;
    private Button mLoadFromTemplateButton;

    private String BOX_ID = "";
    private String BOX_TITLE = "";

    private ConstraintLayout layout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TechCardViewModel viewModel = new ViewModelProvider(requireActivity()).get(TechCardViewModel.class);

        Observer<Template> observer = template -> {
            Log.d("Chosen template: ", template.toString());

            mAirHumiditySlider.setValue((float) template.getHumidityMax());
            mAirTemperatureSlider.setValues((float) template.getTemperatureMin(), (float) template.getTemperatureMax());
            mFluidLevelSlider.setValue(template.getFluidLevelMax());
            mSoilMoistureSlider.setValues((float) template.getSoilMoistureMin(), (float) template.getSoilMoistureMax());
        };

        viewModel.getTemplateData().observe(this, observer);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tech_card_fragment, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mQueue = Volley.newRequestQueue(requireContext());

        String TOKEN = requireActivity().getIntent().getStringExtra("token");

        layout = view.findViewById(R.id.tech_card_layout);

        layout.getForeground().setAlpha(0);

        mSubHeaderText = view.findViewById(R.id.tech_card_sub_header);
        mDayBeginInput = view.findViewById(R.id.input_day_begin);
        mDayEndInput = view.findViewById(R.id.input_day_end);
        mAirHumiditySlider = view.findViewById(R.id.humidity_slider);
        mAirTemperatureSlider = view.findViewById(R.id.temperature_slider);
        mFluidLevelSlider = view.findViewById(R.id.fluid_level_slider);
        mSoilMoistureSlider = view.findViewById(R.id.soil_moisture_slider);
        mSendButton = view.findViewById(R.id.tech_card_button);
        mDeleteButton = view.findViewById(R.id.delete_tech_card_button);
        mLoadFromTemplateButton = view.findViewById(R.id.load_from_template_button);

        mSendButton.setEnabled(false);
        mSendButton.setAlpha(0.5F);

        BOX_ID = getBoxState("id");
        BOX_TITLE = getBoxState("title");

        getCurrentTechCard(TOKEN, BOX_ID);

        mSubHeaderText.setText(getString(R.string.sensors_data_sub_header, BOX_TITLE));

        EditText[] texts = {mDayBeginInput, mDayEndInput};

        CustomTextWatcher textWatcher = new CustomTextWatcher(texts);

        for (EditText text : texts) {
            text.addTextChangedListener(textWatcher);
        }

        mSendButton.setOnClickListener(view1 -> {
            sendTechCardToServer(TOKEN, BOX_ID,
                    parseIntFromString(String.valueOf(mDayBeginInput.getText())),
                    parseIntFromString(String.valueOf(mDayEndInput.getText())),
                    mFluidLevelSlider.getValueFrom(), mFluidLevelSlider.getValue(),
                    getRangeSliderValues(mAirTemperatureSlider, 0),
                    getRangeSliderValues(mAirTemperatureSlider, 1),
                    mAirHumiditySlider.getValueFrom(),  mAirHumiditySlider.getValue(),
                    getRangeSliderValues(mSoilMoistureSlider, 0),
                    getRangeSliderValues(mSoilMoistureSlider, 1));
        });

        mDeleteButton.setOnClickListener(view1 -> {
            showPopupWindow(view, TOKEN, BOX_ID);
        });

        mLoadFromTemplateButton.setOnClickListener(view1 -> {
            requireActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.main_container, new TemplateFragment())
                    .addToBackStack("template")
                    .commit();
        });
    }

    private void sendTechCardToServer(String token, String boxId, int brightnessMin, int brightnessMax,
                                      float fluidLevelMin, float fluidLevelMax, float temperatureMin,
                                      float temperatureMax, float humidityMin, float humidityMax,
                                      float soilMoistureMin, float soilMoistureMax) {

        String url = BuildConfig.SERVER_URL + "/card/edit/" + boxId;
        JSONObject putData = new JSONObject();

        try {
            putData.put("brightnessMin", brightnessMin);
            putData.put("brightnessMax", brightnessMax);
            putData.put("fluidLevelMin", fluidLevelMin);
            putData.put("fluidLevelMax", fluidLevelMax);
            putData.put("temperatureMin", temperatureMin);
            putData.put("temperatureMax", temperatureMax);
            putData.put("humidityMin", humidityMin);
            putData.put("humidityMax", humidityMax);
            putData.put("soilMoistureMin", soilMoistureMin);
            putData.put("soilMoistureMax", soilMoistureMax);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        CustomJsonObjectRequest request = new CustomJsonObjectRequest(Request.Method.PUT, url, putData, response -> {
            Log.e("tech card: ", putData.toString());
        }, Throwable::printStackTrace) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + token);

                return headerMap;
            }
        };

        mQueue.add(request);
    }

    private void getCurrentTechCard(String TOKEN, String boxId) {
        String url = BuildConfig.SERVER_URL + "/card/get/" + boxId;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, response -> {
            try {
                JSONObject object = response.getJSONObject("techCard");

                mDayBeginInput.setHint(String.valueOf(object.getInt("brightnessMin")));
                mDayEndInput.setHint(String.valueOf(object.getInt("brightnessMax")));
                mAirHumiditySlider.setValue((float) object.getDouble("humidityMax"));
                mAirTemperatureSlider.setValues((float) object.getDouble("temperatureMin"), (float) object.getDouble("temperatureMax"));
                mFluidLevelSlider.setValue(object.getInt("fluidLevelMax"));
                mSoilMoistureSlider.setValues((float) object.getDouble("soilMoistureMin"), (float) object.getDouble("soilMoistureMax"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, Throwable::printStackTrace) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + TOKEN);

                return headerMap;
            }
        };

        mQueue.add(request);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void showPopupWindow(View view, String token, String boxId) {
        LayoutInflater inflater = (LayoutInflater) requireActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.confirmation_fragment, null);

        Button mAcceptButton = popupView.findViewById(R.id.accept_button);
        Button mDenyButton = popupView.findViewById(R.id.deny_button);

        int width = ConstraintLayout.LayoutParams.WRAP_CONTENT;
        int height = ConstraintLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = false;

        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        layout.getForeground().setAlpha(128);

        popupWindow.setOnDismissListener(() -> {
            layout.getForeground().setAlpha(0);
        });

        mAcceptButton.setOnClickListener(view1 -> {
            sendTechCardToServer(token, boxId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            popupWindow.dismiss();
        });

        mDenyButton.setOnClickListener(view1 -> {
            popupWindow.dismiss();
        });
    }

    private int parseIntFromString(String str) {
        int res;
        try {
            res = Integer.parseInt(str);

            return res;
        } catch (NumberFormatException e) {
            res = 0;

            return res;
        }
    }

    private float getRangeSliderValues(RangeSlider slider, int position) {
        List<Float> values = slider.getValues();

        return values.get(position);
    }

    private String getBoxState(String tag) {
        SharedPreferences sharedPreferences = requireActivity().getSharedPreferences("boxStateData", Context.MODE_PRIVATE);

        return sharedPreferences.getString(tag, "");
    }

    private class CustomTextWatcher implements TextWatcher {

        private EditText[] texts;

        public CustomTextWatcher(EditText[] texts) {
            this.texts = texts;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            for (EditText text : texts) {
                if (text.getText().toString().trim().length() <= 0) {
                    setButtonState(false);

                    break;
                } else {
                    if (parseIntFromString(text.getText().toString()) >= 0 &&
                            parseIntFromString(text.getText().toString()) <= 23) {
                        setButtonState(true);
                    }
                }
            }
        }

        void setButtonState(boolean state) {
            if (state) {
                mSendButton.setEnabled(true);
                mSendButton.setAlpha(1);
            } else {
                mSendButton.setEnabled(false);
                mSendButton.setAlpha(0.5F);
            }
        }
    }
}