package com.example.eruca.ui.account.features;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.eruca.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FaqFragment extends Fragment {

    private ImageButton mBackButton;
    private ExpandableListView listView;

    private List<String> listHeaders;
    private HashMap<String, List<String>> listDetails;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.faq_fragment, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        BottomNavigationView navView = requireActivity().findViewById(R.id.nav_view);
        navView.setVisibility(View.GONE);

        mBackButton = view.findViewById(R.id.back_button);
        listView = view.findViewById(R.id.faq_list);

        initExpandableLists();

        CustomExpandableListAdapter adapter = new CustomExpandableListAdapter(
                requireContext(), listHeaders, listDetails);

        listView.setAdapter(adapter);

        mBackButton.setOnClickListener(view1 -> requireActivity().onBackPressed());
    }

    private void initExpandableLists() {
        String[] mFaq = requireContext().getResources().getStringArray(R.array.faq_themes);
        String[] mFaqQuestions;
        String[] mFaqAnswers;

        mFaqQuestions = Arrays.copyOfRange(mFaq, 0, (mFaq.length / 2));
        mFaqAnswers = Arrays.copyOfRange(mFaq, (mFaq.length / 2), mFaq.length);

        listDetails = new HashMap<>();

        for (int i = 0; i < mFaq.length / 2; i++) {
            List<String> list = new ArrayList<>();
            list.add(mFaqAnswers[i]);
            listDetails.put(mFaqQuestions[i], list);
        }

        listHeaders = new ArrayList<>(listDetails.keySet());
    }
}
