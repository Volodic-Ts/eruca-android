package com.example.eruca.ui.account;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.eruca.BuildConfig;
import com.example.eruca.R;
import com.example.eruca.auth.LoginActivity;
import com.example.eruca.room.DatabaseController;
import com.example.eruca.ui.account.features.FaqFragment;
import com.example.eruca.ui.account.features.QrScannerFragment;
import com.google.android.material.imageview.ShapeableImageView;

import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class AccountFragment extends Fragment {

    private static final int PICK_IMAGE = 1000;
    private static final int REQUEST_PERMISSION_REQUEST_CODE = 1001;

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    private RequestQueue mQueue;

    private TextView mAccountName;
    private TextView mEmail;

    private CardView mAddBoxCard;
    private CardView mPickImageCard;
    private CardView mFAQCard;
    private CardView mLogOutCard;

    private ShapeableImageView mAccountImage;
    private Uri mImageUri;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.account_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mQueue = Volley.newRequestQueue(requireContext());

        mSharedPreferences = requireContext().getSharedPreferences("app_", Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();

        Log.e("saved path", mSharedPreferences.getString("image", ""));

        String TOKEN = requireActivity().getIntent().getStringExtra("token");
        String LOGIN = requireActivity().getIntent().getStringExtra("login");

        getUserDetails(TOKEN, LOGIN);

        mAccountName = view.findViewById(R.id.profile_name);
        mEmail = view.findViewById(R.id.profile_email);

        mAddBoxCard = view.findViewById(R.id.add_new_box_card);
        mFAQCard = view.findViewById(R.id.faq_card);
        mPickImageCard = view.findViewById(R.id.take_a_photo_card);
        mLogOutCard = view.findViewById(R.id.log_out_card);
        mAccountImage = view.findViewById(R.id.account_image);

        String mUri = mSharedPreferences.getString("image", null);

        restoreSavedState(mUri);

        mPickImageCard.setOnClickListener(view1 -> {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
        });

        mAddBoxCard.setOnClickListener(view1 -> {
            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                requireActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.main_container, new QrScannerFragment())
                        .addToBackStack("qr")
                        .commit();
            } else {
                ActivityCompat.requestPermissions(requireActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        REQUEST_PERMISSION_REQUEST_CODE);
            }
        });

        mFAQCard.setOnClickListener(view1 -> {
            requireActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.main_container, new FaqFragment())
                    .addToBackStack("faq")
                    .commit();
        });

        mLogOutCard.setOnClickListener(view1 -> {
            SharedPreferences mPreferences = requireContext().getSharedPreferences("remember_me", Context.MODE_PRIVATE);
            SharedPreferences.Editor mEditor = mPreferences.edit();

            DatabaseController controller = new DatabaseController(requireActivity());
            controller.removeSavedUserFromDatabase(LOGIN);

            mEditor.putString("flag", "false");
            mEditor.putString("login", "");
            mEditor.apply();

            requireActivity().finish();
            startActivity(new Intent(requireContext(), LoginActivity.class));
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_REQUEST_CODE) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), permission);
                    if (showRationale) {
                        Toast.makeText(requireContext(), "Denied once or more", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(requireContext(), "Never ask again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    requireActivity().getSupportFragmentManager().beginTransaction()
                            .add(R.id.main_container, new QrScannerFragment())
                            .addToBackStack("qr")
                            .commit();
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == PICK_IMAGE) {
            if (data != null) {
                mImageUri = data.getData();
                Log.e("path:: ", String.valueOf(mImageUri));
                mEditor.putString("image", String.valueOf(mImageUri));
                mEditor.commit();

                Glide.with(this)
                        .load(mImageUri.toString())
                        .into(mAccountImage);
            }
        } else {
            Toast.makeText(getContext(), "You haven't pick image", Toast.LENGTH_SHORT).show();
        }
    }

    private void restoreSavedState(String url) {
        if (url != null && !url.isEmpty()) {
            Glide.with(this)
                    .load(url)
                    .into(mAccountImage);
        }
    }

    private void getUserDetails(String token, String accountName) {
        String url = BuildConfig.SERVER_URL + "/user/get/" + accountName;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, response -> {
            try {
                String email = response.getString("email");

                mAccountName.setText(getString(R.string.account_name, accountName));
                mEmail.setText(getString(R.string.account_email, email));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, Throwable::printStackTrace) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + token);

                return headerMap;
            }
        };

        mQueue.add(request);
    }
}
