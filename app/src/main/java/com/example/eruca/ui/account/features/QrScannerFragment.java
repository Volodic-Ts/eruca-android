package com.example.eruca.ui.account.features;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.example.eruca.BuildConfig;
import com.example.eruca.MainActivity;
import com.example.eruca.R;
import com.example.eruca.heplers.CustomJsonObjectRequest;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QrScannerFragment extends Fragment {

    private RequestQueue mQueue;

    private CodeScanner mCodeScanner;
    private TextView mBoxTitle;
    private TextView mBoxVersion;

    private View view;
    String TOKEN = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.qr_scanner_fragment, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TOKEN = requireActivity().getIntent().getStringExtra("token");
        this.view = view;
        mQueue = Volley.newRequestQueue(requireContext());

        BottomNavigationView navView = requireActivity().findViewById(R.id.nav_view);
        navView.setVisibility(View.GONE);

//        mCodeScanner = view.findViewById(R.id.qr_scanner);
        mBoxTitle = view.findViewById(R.id.box_title);
        mBoxVersion = view.findViewById(R.id.box_version);

        scanQrCode(view, TOKEN);

//        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
//            ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.CAMERA}, REQUEST_PERMISSION_REQUEST_CODE);
//        } else {
//        }
    }

    private void addNewBoxToAccount(String TOKEN, String boxTitle, String boxVersion) {
        String url = BuildConfig.SERVER_URL + "/boxes/add";

        JSONObject postData = new JSONObject();

        try {
            postData.put("title", boxTitle);
            postData.put("version", boxVersion);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        CustomJsonObjectRequest jsonObjectRequest = new CustomJsonObjectRequest(Request.Method.POST, url, postData, response -> {
            Toast.makeText(requireContext(), getString(R.string.added_box_toast), Toast.LENGTH_LONG).show();

            new Handler().postDelayed(() -> {
                requireActivity().onBackPressed();

            }, 1300);

        }, Throwable::printStackTrace) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerMap = new HashMap<>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + TOKEN);

                return headerMap;
            }
        };

        mQueue.add(jsonObjectRequest);
    }

    private void scanQrCode(View view, String TOKEN) {
//        List<String> boxDetails = new ArrayList<>();

        CodeScannerView scannerView = view.findViewById(R.id.qr_scanner);
        mCodeScanner = new CodeScanner(requireActivity(), scannerView);

        mCodeScanner.setDecodeCallback(result -> requireActivity().runOnUiThread(() -> {
            String qrText = result.getText();

            addNewBoxToAccount(TOKEN, scannedStringParser(qrText).get(0), scannedStringParser(qrText).get(1));

            mBoxTitle.setText(getString(R.string.qr_box_title, scannedStringParser(qrText).get(0)));
            mBoxVersion.setText(getString(R.string.qr_box_version, scannedStringParser(qrText).get(1)));

        }));
        scannerView.setOnClickListener(view1 -> mCodeScanner.startPreview());
    }

    @Override
    public void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    public void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }

    private List<String> scannedStringParser(String str) {
        List<String> list = new ArrayList<>();
        int i = str.indexOf(":");
        list.add(str.substring(0, i));
        list.add(str.substring(i + 1));

        return list;
    }

}
