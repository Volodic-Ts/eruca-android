package com.example.eruca.ui.modulesState;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eruca.BuildConfig;
import com.example.eruca.R;
import com.example.eruca.boxes.Box;
import com.example.eruca.boxes.BoxViewModel;
import com.example.eruca.heplers.CustomJsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@SuppressLint("UseSwitchCompatOrMaterialCode")
public class ModulesStateFragment extends Fragment {

    private RequestQueue mQueue;
    private RequestQueue mStatesQueue;

    private Switch mCoolerSwitch;
    private Switch mPumpSwitch;
    private Switch mLightSwitch;

    private ImageView mCoolerImage;
    private ImageView mPumpImage;
    private ImageView mLightImage;

    private TextView mSubHeaderText;

    private String BOX_ID = "";
    private String BOX_TITLE = "";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.modules_state_fragment, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mQueue = Volley.newRequestQueue(requireContext());
        mStatesQueue = Volley.newRequestQueue(requireContext());

        String TOKEN = requireActivity().getIntent().getStringExtra("token");

        mSubHeaderText = view.findViewById(R.id.modules_state_sub_header);

        mCoolerSwitch = view.findViewById(R.id.cooler_switch);
        mPumpSwitch = view.findViewById(R.id.pump_switch);
        mLightSwitch = view.findViewById(R.id.light_switch);

        mLightImage = view.findViewById(R.id.ic_light);
        mPumpImage = view.findViewById(R.id.ic_pump);
        mCoolerImage = view.findViewById(R.id.ic_cooler);

        BOX_ID = getBoxState("id");
        BOX_TITLE = getBoxState("title");

        mSubHeaderText.setText(getString(R.string.sensors_data_sub_header, BOX_TITLE));

        mCoolerSwitch.setOnCheckedChangeListener(new SetSwitchListener(getContext(), mCoolerSwitch, mCoolerImage, R.drawable.ic_fan_on, R.drawable.ic_fan_off, TOKEN));
        mPumpSwitch.setOnCheckedChangeListener(new SetSwitchListener(getContext(), mPumpSwitch, mPumpImage, R.drawable.ic_humidity_high_on, R.drawable.ic_humidity_high_off, TOKEN));
        mLightSwitch.setOnCheckedChangeListener(new SetSwitchListener(getContext(), mLightSwitch, mLightImage, R.drawable.ic_light_on, R.drawable.ic_light_off, TOKEN));

        getSwitchStates(TOKEN, BOX_ID);
    }

    private void sendSwitchStateToServer(String token, String boxId) {
        String url = BuildConfig.SERVER_URL + "/modules/edit/" + boxId;
        JSONObject putData = new JSONObject();

        try {
            putData.put("light", mLightSwitch.isChecked());
            putData.put("pump", mPumpSwitch.isChecked());
            putData.put("cooler", mCoolerSwitch.isChecked());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        CustomJsonObjectRequest request = new CustomJsonObjectRequest(Request.Method.PUT, url, putData, response -> {

        }, Throwable::printStackTrace) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + token);

                return headerMap;
            }
        };

        mQueue.add(request);
    }

    private void getSwitchStates(String token, String boxId) {
        String url = BuildConfig.SERVER_URL + "/modules/get/" + boxId;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, response -> {
            try {
                mPumpSwitch.setChecked(response.getBoolean("pump"));
                mLightSwitch.setChecked(response.getBoolean("light"));
                mCoolerSwitch.setChecked(response.getBoolean("cooler"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, Throwable::printStackTrace) {
                @Override
                public Map<String, String> getHeaders() {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + token);

                return headerMap;
            }
        };

        mStatesQueue.add(request);
    }

    private String getBoxState(String tag) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("boxStateData", Context.MODE_PRIVATE);

        return sharedPreferences.getString(tag, "");
    }

    private class SetSwitchListener implements CompoundButton.OnCheckedChangeListener {
        private Context mContext;
        Switch mSwitch;
        ImageView mImageView;
        int imageKey, alternateImageKey;
        String token;

        public SetSwitchListener(Context mContext, Switch mSwitch, ImageView mImageView, int imageKey, int alternateImageKey, String token) {
            this.mContext = mContext;
            this.mSwitch = mSwitch;
            this.mImageView = mImageView;
            this.imageKey = imageKey;
            this.alternateImageKey = alternateImageKey;
            this.token = token;
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            sendSwitchStateToServer(token, BOX_ID);

            if (b) {
                mImageView.setImageResource(imageKey);
            } else {
                mImageView.setImageResource(alternateImageKey);
            }
        }
    }
}