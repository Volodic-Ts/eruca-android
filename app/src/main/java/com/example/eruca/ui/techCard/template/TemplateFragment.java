package com.example.eruca.ui.techCard.template;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eruca.BuildConfig;
import com.example.eruca.R;
import com.example.eruca.heplers.OnTemplateClickListener;
import com.example.eruca.heplers.TemplatesAdapter;
import com.example.eruca.heplers.model.Template;
import com.example.eruca.ui.techCard.TechCardViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TemplateFragment extends Fragment {

    private RequestQueue mQueue;

    private RecyclerView mRecyclerView;

    private TechCardViewModel techCardViewModel;

    private List<Template> templateList = new ArrayList<>();
    private TemplatesAdapter adapter = null;

    private Activity mActivity;
    private Context mContext;

    private OnTemplateClickListener onTemplateClickListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        techCardViewModel = new ViewModelProvider(requireActivity()).get(TechCardViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.tech_card_templates_fragment, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mQueue = Volley.newRequestQueue(requireContext());

        String TOKEN = requireActivity().getIntent().getStringExtra("token");

        BottomNavigationView navView = requireActivity().findViewById(R.id.nav_view);
        navView.setVisibility(View.GONE);

        mActivity = requireActivity();
        mContext = mActivity.getApplicationContext();

        mRecyclerView = view.findViewById(R.id.templates_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

        parseTemplatesDataFromDB(TOKEN);

        onTemplateClickListener = (template, position) -> {
            techCardViewModel.setTemplateData(template);

            requireActivity().onBackPressed();

            Log.d("Clicked template: ", String.valueOf(position));
            Log.d("Current template: ", String.valueOf(template.getPlantId()));
        };
    }

    private void parseTemplatesDataFromDB(String TOKEN) {
        String url = BuildConfig.SERVER_URL + "/templates/get/all";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, response -> {
            try {
                JSONArray array = response.getJSONArray("templates");

                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);

                    Template template = new Template(object.getInt("plantId"),
                            object.getString("plantName"),
                            object.getString("plantDescription"),
                            object.getString("plantImageUrl"),
                            object.getInt("fluidLevelMin"),
                            object.getInt("fluidLevelMax"),
                            object.getDouble("temperatureMin"),
                            object.getDouble("temperatureMax"),
                            object.getDouble("humidityMin"),
                            object.getDouble("humidityMax"),
                            object.getDouble("soilMoistureMin"),
                            object.getDouble("soilMoistureMax"));

                    templateList.add(template);
                }

                adapter = new TemplatesAdapter(mContext, mActivity, templateList, onTemplateClickListener);
                mRecyclerView.setAdapter(adapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, Throwable::printStackTrace) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + TOKEN);

                return headerMap;
            }
        };

        mQueue.add(request);
    }
}
