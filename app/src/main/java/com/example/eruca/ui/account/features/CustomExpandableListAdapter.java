package com.example.eruca.ui.account.features;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.eruca.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<String> groupData;
    private HashMap<String, List<String>> childData;
//    private ArrayList<ArrayList<String>> childData;

    public CustomExpandableListAdapter(Context mContext, List<String> groupData, HashMap<String, List<String>> childData) {
        this.mContext = mContext;
        this.groupData = groupData;
        this.childData = childData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.childData.get(this.groupData.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int childPosition, int groupPosition) {
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.childData.get(this.groupData.get(groupPosition)).size();
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean b, View view, ViewGroup viewGroup) {
        String str = getChild(groupPosition, childPosition).toString();
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.expandable_list_item, null);
        }

        TextView textListChild = (TextView) view.findViewById(R.id.text1);
        textListChild.setText(str);

        return view;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupData.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public int getGroupCount() {
        return groupData.size();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.expandable_group_item, null);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.text1);
        textView.setText(groupData.get(groupPosition));

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
