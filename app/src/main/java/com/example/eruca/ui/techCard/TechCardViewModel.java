package com.example.eruca.ui.techCard;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.eruca.heplers.model.Template;

public class TechCardViewModel extends ViewModel {

    private MutableLiveData<Template> data;

    public void setTemplateData(Template template) {
        data.setValue(template);
    }

    public MutableLiveData<Template> getTemplateData() {
        if (data == null) {
            data = new MutableLiveData<>();
        }

        return data;
    }
}