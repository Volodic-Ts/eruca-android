package com.example.eruca.ui.sensorsData.manualControl;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.eruca.R;
import com.example.eruca.heplers.ManualControlController;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class AirHumidityFragment extends Fragment {

    private ImageButton mBackButton;
    private EditText mHumidityMinText;
    private EditText mHumidityMaxText;
    private Button mSaveButton;

    private ManualControlController controller;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.humidity_manual_control_fragment, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        BottomNavigationView navView = requireActivity().findViewById(R.id.nav_view);
        navView.setVisibility(View.GONE);

        mBackButton = view.findViewById(R.id.back_button);
        mSaveButton = view.findViewById(R.id.manual_humidity_save_button);
        mHumidityMinText = view.findViewById(R.id.humidity_input_first);
        mHumidityMaxText = view.findViewById(R.id.humidity_input_second);

        ImageView mImageView1 = view.findViewById(R.id.input_1_back);
        ImageView mImageView2 = view.findViewById(R.id.input_2_back);

        mSaveButton.setEnabled(false);
        mSaveButton.setAlpha(0.5F);

        controller = new ManualControlController(mSaveButton, mHumidityMinText, mHumidityMaxText, mImageView1, mImageView2);
        controller.validateFields();

        mBackButton.setOnClickListener(view1 -> requireActivity().onBackPressed());

        mSaveButton.setOnClickListener(view1 -> {

        });
    }

    private double parseDataFromFields(String str) {
        double d;
        try {
            d = Double.parseDouble(str);
        } catch (NumberFormatException e) {
            return 0;
        }

        return d;
    }
}
