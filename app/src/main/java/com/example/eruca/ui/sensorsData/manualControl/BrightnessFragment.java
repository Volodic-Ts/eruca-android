package com.example.eruca.ui.sensorsData.manualControl;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.eruca.R;
import com.example.eruca.heplers.ManualControlController;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class BrightnessFragment extends Fragment {

    private ImageButton mBackButton;
    private EditText mBrightnessMinText;
    private EditText mBrightnessMaxText;
    private Button mSaveButton;

    private ManualControlController controller;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.brightness_manual_control_fragment, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        BottomNavigationView navView = requireActivity().findViewById(R.id.nav_view);
        navView.setVisibility(View.GONE);

        mBackButton = view.findViewById(R.id.back_button);
        mSaveButton = view.findViewById(R.id.manual_brightness_save_button);
        mBrightnessMinText = view.findViewById(R.id.brightness_input_first);
        mBrightnessMaxText = view.findViewById(R.id.brightness_input_second);

        ImageView mImageView1 = view.findViewById(R.id.input_1_back);
        ImageView mImageView2 = view.findViewById(R.id.input_2_back);

        mSaveButton.setEnabled(false);
        mSaveButton.setAlpha(0.5F);

        Log.d("BackStack:: ", String.valueOf(requireActivity().getSupportFragmentManager().getBackStackEntryCount()));

        controller = new ManualControlController(mSaveButton, mBrightnessMinText, mBrightnessMaxText, mImageView1, mImageView2);
        controller.validateFields();

        mBackButton.setOnClickListener(view1 -> requireActivity().onBackPressed());
    }
}
