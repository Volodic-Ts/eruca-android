package com.example.eruca.auth;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eruca.BuildConfig;
import com.example.eruca.R;
import com.example.eruca.heplers.CustomJsonObjectRequest;

import org.json.JSONException;

public class VerificationFragment extends Fragment {

    private RequestQueue mQueue;

    private EditText mVer1;
    private EditText mVer2;
    private EditText mVer3;
    private EditText mVer4;
    private EditText mVer5;
    private EditText mVer6;
    private EditText mVer7;
    private EditText mVer8;

    private Button mButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.verification_fragment, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mQueue = Volley.newRequestQueue(getContext());

        mVer1 = view.findViewById(R.id.ver_1);
        mVer2 = view.findViewById(R.id.ver_2);
        mVer3 = view.findViewById(R.id.ver_3);
        mVer4 = view.findViewById(R.id.ver_4);
        mVer5 = view.findViewById(R.id.ver_5);
        mVer6 = view.findViewById(R.id.ver_6);
        mVer7 = view.findViewById(R.id.ver_7);
        mVer8 = view.findViewById(R.id.ver_8);

        mButton = view.findViewById(R.id.verify_button);

        mButton.setEnabled(false);
        mButton.setAlpha(0.5F);

        EditText[] fields = {mVer1, mVer2, mVer3, mVer4, mVer5, mVer6, mVer7, mVer8};

        for (EditText text : fields) {
            text.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (!mVer8.getText().toString().equals("")) {
                        text.clearFocus();
                        mButton.setAlpha(1);
                        mButton.setEnabled(true);
                    } else {
                        goToNextView().requestFocus();
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }

                public EditText goToNextView() {
                    int i;

                    for(i = 0; i < fields.length - 1; i++) {
                        if (fields[i] == text) {
                            return fields[i + 1];
                        }
                    }
                    return fields[i];
                }
            });
        }

        mButton.setOnClickListener(view1 -> {
            String code = mVer1.getText().toString() + mVer2.getText().toString() +
                    mVer3.getText().toString() + mVer4.getText().toString() +
                    mVer5.getText().toString() + mVer6.getText().toString() +
                    mVer7.getText().toString() + mVer8.getText().toString();

            verifyAccount(code);
        });
    }

    private void verifyAccount(String code) {
        String url = BuildConfig.SERVER_URL + "/confirm/" + code;

        CustomJsonObjectRequest request = new CustomJsonObjectRequest(Request.Method.POST, url, null, response -> {
            Toast.makeText(getContext(), "Successfully!", Toast.LENGTH_SHORT).show();

            FragmentManager manager = getActivity().getSupportFragmentManager();
            Fragment fragment = manager.findFragmentByTag("registration");

            assert fragment != null;
            getActivity().getSupportFragmentManager().beginTransaction()
                    .remove(this)
                    .remove(fragment)
                    .add(R.id.login_container, new LoginFragment(), "login")
                    .commit();
        }, error -> {

        });

        mQueue.add(request);
    }
}
