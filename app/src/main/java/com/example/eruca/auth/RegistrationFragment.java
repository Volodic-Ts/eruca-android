package com.example.eruca.auth;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.eruca.BuildConfig;
import com.example.eruca.R;
import com.example.eruca.heplers.CustomJsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class RegistrationFragment extends Fragment {

    private RequestQueue mQueue;

    private EditText mLoginText;
    private EditText mEmailText;
    private EditText mPasswordText;
    private EditText mRetypePasswordText;

    private TextView mPasswordRequirements;

    private Button mSendButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.registration_fragment, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mQueue = Volley.newRequestQueue(getContext());

        mLoginText = view.findViewById(R.id.login_input);
        mEmailText = view.findViewById(R.id.email_input);
        mPasswordText = view.findViewById(R.id.password_input);
        mRetypePasswordText = view.findViewById(R.id.retype_input);

        mPasswordRequirements = view.findViewById(R.id.password_requirements);

        mPasswordRequirements.setVisibility(View.INVISIBLE);

        EditText[] fields = {mLoginText, mEmailText, mPasswordText, mRetypePasswordText};

        mSendButton = view.findViewById(R.id.reg_button);

        mSendButton.setEnabled(false);
        mSendButton.setAlpha(0.5F);

        CustomTextWatcher textWatcher = new CustomTextWatcher(mSendButton, fields);

        for (EditText text : fields) {
            text.addTextChangedListener(textWatcher);
        }

        mSendButton.setOnClickListener(view1 -> {
            if (mPasswordText.getText().toString().equals("")) {
                mPasswordText.setError(getString(R.string.empty_field_error));
            } else if (!mPasswordText.getText().toString().equals(mRetypePasswordText.getText().toString())) {
                mRetypePasswordText.setError(getString(R.string.matches_passwords));
            } else {
                sendDataForRegistration(mLoginText.getText().toString(), mEmailText.getText().toString(),
                        mPasswordText.getText().toString(), mRetypePasswordText.getText().toString());
            }
        });


    }

    private void sendDataForRegistration(String login, String email, String password, String password2) {
        String url = BuildConfig.SERVER_URL + "/reg";
//        String url = "https://webhook.site/aaf561e5-d73e-4580-8fb7-5a1d9b6e2928";
        JSONObject postData = new JSONObject();

        Log.e("data:", login + ", " + email + ", " + password + ", " + password2);

        try {
            postData.put("login", login);
            postData.put("email", email);
            postData.put("password", password);
            postData.put("matchPassword", password2);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("post data: ", postData.toString());

        CustomJsonObjectRequest request = new CustomJsonObjectRequest(Request.Method.POST, url, postData, response -> {

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.login_container, new VerificationFragment(), "verification")
                    .addToBackStack(null)
                    .commit();

        }, Throwable::printStackTrace);

        mQueue.add(request);
    }

    private class CustomTextWatcher implements TextWatcher {

        Button button;
        EditText[] texts;
        int type;

        public CustomTextWatcher(Button button, EditText[] texts) {
            this.button = button;
            this.texts = texts;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            for (EditText text : texts) {
                if (text.getText().toString().trim().length() <= 0) {
                    setButtonState(false);

                    break;
                } else {
                    if (text == mEmailText && Patterns.EMAIL_ADDRESS.matcher(text.getText()).matches()) {
                        setButtonState(true);
                    }
                    if ((text == mRetypePasswordText && text.getText() == mPasswordText.getText()) &&
                            mPasswordText.getText().toString().length() >= 8 && mPasswordText.getText().toString().length() <= 16) {
                        mPasswordRequirements.setVisibility(View.INVISIBLE);
                        setButtonState(true);
                    }
                }
            }
        }

        void setButtonState(boolean state) {
            if (state) {
                mSendButton.setEnabled(true);
                mSendButton.setAlpha(1);
            } else {
                button.setEnabled(false);
                button.setAlpha(0.5F);
            }
        }
    }
}
