package com.example.eruca.auth;

import static android.content.Context.MODE_PRIVATE;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eruca.BuildConfig;
import com.example.eruca.MainActivity;
import com.example.eruca.R;
import com.example.eruca.room.AppExecutors;
import com.example.eruca.room.DatabaseController;
import com.example.eruca.room.database.UserDatabase;
import com.example.eruca.room.entity.UserTable;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginFragment extends Fragment {

    private RequestQueue mQueue;

    private TextView wrongDataText;
    private Button mSignInButton;
    private EditText mLoginText;
    private EditText mPasswordText;
    private CheckBox mRememberCheckBox;
    private TextView mSignUp;

    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    private DatabaseController controller;

    private String TOKEN = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.login_fragment, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mQueue = Volley.newRequestQueue(getContext());

        controller = new DatabaseController(requireActivity());

        mLoginText = view.findViewById(R.id.login_input);
        mPasswordText = view.findViewById(R.id.password_input);
        mRememberCheckBox = view.findViewById(R.id.remember_checkBox);
        mSignInButton = view.findViewById(R.id.sign_in_button);
        wrongDataText = view.findViewById(R.id.wrong_data_text);
        mSignUp = view.findViewById(R.id.sign_up);

        wrongDataText.setVisibility(View.INVISIBLE);
        mLoginText.setBackgroundResource(R.drawable.input_account_data_field);
        mPasswordText.setBackgroundResource(R.drawable.input_account_data_field);

        mPreferences = getContext().getSharedPreferences("remember_me", MODE_PRIVATE);
//        boolean flag = mPreferences.getBoolean("flag", false);
//        Log.e("Checkbox: ", String.valueOf(flag));

        mSignInButton.setOnClickListener(view1 -> {
            if (mLoginText.getText().toString().equals("")) {
                mLoginText.setError(getString(R.string.empty_field_error));
            } else if (mPasswordText.getText().toString().equals("")) {
                mPasswordText.setError(getString(R.string.empty_field_error));
            } else {
                makeAuthRequest(mLoginText.getText().toString(), mPasswordText.getText().toString());
            }
        });

        mRememberCheckBox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked() && mLoginText.getText().length() > 0 && mPasswordText.getText().length() > 0) {
                controller.saveRememberedUserToDatabase(mLoginText.getText().toString(), mPasswordText.getText().toString());
                mEditor = mPreferences.edit();
                mEditor.putString("flag", "true");
                mEditor.putString("login", mLoginText.getText().toString());
                mEditor.apply();
            } else if (!compoundButton.isChecked()) {
                mEditor = mPreferences.edit();
                mEditor.putString("flag", "false");
                mEditor.apply();
            }
        });

        mSignUp.setOnClickListener(view1 -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(R.id.login_container, new RegistrationFragment(), "registration")
                    .addToBackStack(null)
                    .commit();
        });
    }

    private void makeAuthRequest(String login, String password) {
        String url = BuildConfig.SERVER_URL + "/auth";

        JSONObject postData = new JSONObject();

        try {
            postData.put("username", login);
            postData.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, postData, response -> {
            try {
                TOKEN = response.getString("jwt");

                Intent intent = new Intent(getContext(), MainActivity.class);
                intent.putExtra("token", TOKEN);
                intent.putExtra("login", login);

                startActivity(intent);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> {
            wrongDataText.setVisibility(View.VISIBLE);
            Log.e("Error: ", postData.toString());
            error.printStackTrace();
        });

        mQueue.add(request);
    }

//    private void saveUserDataToDB(String login, String password, boolean flag) {
//        UserTable user = new UserTable(login, password, flag);
//        AppExecutors.getInstance().getDiskIO().execute(new Runnable() {
//            @Override
//            public void run() {
////                userDatabase.userDao().addNewUser(user);
//            }
//        });
////        UserDatabase database = Room.databaseBuilder(requireActivity().getApplicationContext(),
////                UserDatabase.class, "user_db").enableMultiInstanceInvalidation().build();
////
////        UserTable user = new UserTable(login, password, flag);
////        UserDao userDao = database.userDao();
////
////        userDao.insertSavedUser(user);
//    }
}
