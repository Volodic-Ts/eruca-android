package com.example.eruca.heplers;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.eruca.R;
import com.example.eruca.heplers.model.Template;

import java.util.List;

public class TemplatesAdapter extends RecyclerView.Adapter<TemplatesAdapter.TemplatesHolder> {

    private static final int EMPTY_LIST_TYPE = 0;
    private static final int NON_EMPTY_LIST_TYPE = 1;

    private Context mContext;
    private Activity mActivity;
    private List<Template> templateList;

    private final OnTemplateClickListener onTemplateClickListener;

    public TemplatesAdapter(Context mContext, Activity mActivity, List<Template> templateList, OnTemplateClickListener onTemplateClickListener) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.templateList = templateList;
        this.onTemplateClickListener = onTemplateClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return templateList.isEmpty() ? EMPTY_LIST_TYPE : NON_EMPTY_LIST_TYPE;
    }

    @NonNull
    @Override
    public TemplatesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == EMPTY_LIST_TYPE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_no_item, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_raw_item, parent, false);
        }

        return new TemplatesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TemplatesHolder mainHolder, int position) {
        if (getItemViewType(position) == EMPTY_LIST_TYPE) {
            return;
        }

        TemplatesHolder holder = (TemplatesHolder) mainHolder;
        Template template = templateList.get(position);

        String imageUrl = template.getImageUrl();
        if (imageUrl != null && !imageUrl.isEmpty()) {
            Glide.with(mContext)
                    .load(imageUrl)
                    .into(holder.mTemplateImage);
        }
        holder.mTemplateTitle.setText(template.getTitle());
        holder.mTemplateDescription.setText(template.getDescription());

        holder.itemView.setOnClickListener(view -> onTemplateClickListener.onItemClick(template, position));
    }

    @Override
    public int getItemCount() {
        if (templateList.size() == 0) {
            return 1;
        } else {
            return templateList.size();
        }
    }

    public static class TemplatesHolder extends RecyclerView.ViewHolder {
        TextView mTemplateTitle;
        TextView mTemplateDescription;
        ImageView mTemplateImage;

        public TemplatesHolder(@NonNull View itemView) {
            super(itemView);

            mTemplateTitle = itemView.findViewById(R.id.card_title);
            mTemplateDescription = itemView.findViewById(R.id.card_description);
            mTemplateImage = itemView.findViewById(R.id.card_image);
        }
    }
}
