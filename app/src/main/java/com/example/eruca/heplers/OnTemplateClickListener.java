package com.example.eruca.heplers;

import com.example.eruca.heplers.model.Template;

public interface OnTemplateClickListener {
    void onItemClick(Template template, int position);
}
