package com.example.eruca.heplers;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class ManualControlController {
    private Button mButton;
    private EditText mEditText1;
    private EditText mEditText2;
    private ImageView mImageView1;
    private ImageView mImageView2;

    private EditText[] texts;
    private ImageView[] views;

    public ManualControlController(Button mButton, EditText mEditText1, EditText mEditText2, ImageView mImageView1, ImageView mImageView2) {
        this.mButton = mButton;
        this.mEditText1 = mEditText1;
        this.mEditText2 = mEditText2;
        this.mImageView1 = mImageView1;
        this.mImageView2 = mImageView2;
        this.texts = new EditText[] {mEditText1, mEditText2};
        this.views = new ImageView[] {mImageView1, mImageView2};
    }

    public void validateFields() {
        for (int i = 0, j = 1; i < texts.length && j >= 0; i++, j--) {
            int finalI = i;
            int finalJ = j;

            texts[i].setOnFocusChangeListener((view, b) -> {
                if (b) {
                    views[finalI].setVisibility(View.VISIBLE);
                    views[finalJ].setVisibility(View.INVISIBLE);
                }
            });

            texts[i].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int k, int j1, int j2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int k, int j1, int j2) {
                    if (!texts[finalI].getText().toString().isEmpty() && !texts[finalJ].getText().toString().isEmpty()) {
                        mButton.setAlpha(1);
                        mButton.setEnabled(true);
                    } else {
                        mButton.setAlpha(0.5F);
                        mButton.setEnabled(false);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }
    }
}
