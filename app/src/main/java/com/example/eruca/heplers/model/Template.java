package com.example.eruca.heplers.model;

import androidx.annotation.NonNull;

public class Template {

    private int plantId;
    private String title;
    private String description;
    private String imageUrl;
    private int fluidLevelMin;
    private int fluidLevelMax;
    private double temperatureMin;
    private double temperatureMax;
    private double humidityMin;
    private double humidityMax;
    private double soilMoistureMin;
    private double soilMoistureMax;

    public Template(String title, String description, String imageUrl) {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
    }

    public Template(int plantId, String title, String description, String imageUrl,
                    int fluidLevelMin, int fluidLevelMax,
                    double temperatureMin, double temperatureMax,
                    double humidityMin, double humidityMax,
                    double soilMoistureMin, double soilMoistureMax) {
        this.plantId = plantId;
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.fluidLevelMin = fluidLevelMin;
        this.fluidLevelMax = fluidLevelMax;
        this.temperatureMin = temperatureMin;
        this.temperatureMax = temperatureMax;
        this.humidityMin = humidityMin;
        this.humidityMax = humidityMax;
        this.soilMoistureMin = soilMoistureMin;
        this.soilMoistureMax = soilMoistureMax;
    }

    public int getPlantId() {
        return plantId;
    }

    public void setPlantId(int plantId) {
        this.plantId = plantId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getFluidLevelMin() {
        return fluidLevelMin;
    }

    public void setFluidLevelMin(int fluidLevelMin) {
        this.fluidLevelMin = fluidLevelMin;
    }

    public int getFluidLevelMax() {
        return fluidLevelMax;
    }

    public void setFluidLevelMax(int fluidLevelMax) {
        this.fluidLevelMax = fluidLevelMax;
    }

    public double getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(double temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    public double getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(double temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    public double getHumidityMin() {
        return humidityMin;
    }

    public void setHumidityMin(double humidityMin) {
        this.humidityMin = humidityMin;
    }

    public double getHumidityMax() {
        return humidityMax;
    }

    public void setHumidityMax(double humidityMax) {
        this.humidityMax = humidityMax;
    }

    public double getSoilMoistureMin() {
        return soilMoistureMin;
    }

    public void setSoilMoistureMin(double soilMoistureMin) {
        this.soilMoistureMin = soilMoistureMin;
    }

    public double getSoilMoistureMax() {
        return soilMoistureMax;
    }

    public void setSoilMoistureMax(double soilMoistureMax) {
        this.soilMoistureMax = soilMoistureMax;
    }

    @NonNull
    @Override
    public String toString() {
        return "Template{" +
                "plantId=" + plantId +
                ", fluidLevelMin=" + fluidLevelMin +
                ", fluidLevelMax=" + fluidLevelMax +
                ", temperatureMin=" + temperatureMin +
                ", temperatureMax=" + temperatureMax +
                ", humidityMin=" + humidityMin +
                ", humidityMax=" + humidityMax +
                ", soilMoistureMin=" + soilMoistureMin +
                ", soilMoistureMax=" + soilMoistureMax +
                '}';
    }
}
