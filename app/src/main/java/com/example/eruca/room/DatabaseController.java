package com.example.eruca.room;

import android.app.Activity;
import android.content.Context;

import com.example.eruca.room.database.UserDatabase;
import com.example.eruca.room.entity.UserTable;

import java.util.ArrayList;
import java.util.List;

public class DatabaseController {
    private Context mContext;
    private Activity mActivity;
    private UserDatabase userDatabase;

    public DatabaseController(Activity mActivity) {
        userDatabase = UserDatabase.getInstance(mActivity.getApplicationContext());
    }

    public void saveRememberedUserToDatabase(String login, String password) {
        AppExecutors.getInstance().getDiskIO().execute(() -> {
            UserTable userTable = new UserTable(login, password);
            userDatabase.userDao().addNewUser(userTable);
        });
    }

    public String getSavedUserDetailsByLogin(String login) {
        final String[] password = new String[1];
        AppExecutors.getInstance().getDiskIO().execute(() -> {
            UserTable userTable = userDatabase.userDao().getUser(login);
            password[0] = userTable.getUserPassword();
        });
        return password[0];
    }

    public void removeSavedUserFromDatabase(String login) {
        AppExecutors.getInstance().getDiskIO().execute(() -> {
            UserTable userTable = userDatabase.userDao().getUser(login);
            userDatabase.userDao().deleteAddedUser(userTable);
        });
    }
}
