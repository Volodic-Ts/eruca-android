package com.example.eruca.room.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.eruca.room.entity.UserTable;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM usertable WHERE login = :userLogin")
    UserTable getUser(String userLogin);

    @Query("SELECT * FROM usertable")
    UserTable getAllUsers();

    @Insert
    void addNewUser(UserTable user);

    @Update
    void updateSavedUser(UserTable user);

    @Delete
    void deleteAddedUser(UserTable user);
}
