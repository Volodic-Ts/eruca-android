package com.example.eruca;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.eruca.boxes.BoxesFragment;
import com.example.eruca.heplers.ViewPagerAdapter;
import com.example.eruca.ui.account.AccountFragment;
import com.example.eruca.ui.modulesState.ModulesStateFragment;
import com.example.eruca.ui.sensorsData.SensorsDataFragment;
import com.example.eruca.ui.techCard.TechCardFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.viewpager2.widget.ViewPager2;

public class MainActivity extends AppCompatActivity {

    private String TOKEN = "";

    private BottomNavigationView navView;
    private ViewPager2 viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        TOKEN = getIntent().getStringExtra("token");

        Log.e("Success: ", TOKEN);

        navView = findViewById(R.id.nav_view);
        viewPager = findViewById(R.id.bottom_nav_container);

        ViewPagerAdapter adapter = new ViewPagerAdapter(this);
        adapter.addFragment(new SensorsDataFragment());
        adapter.addFragment(new ModulesStateFragment());
        adapter.addFragment(new TechCardFragment());
        adapter.addFragment(new AccountFragment());
        viewPager.setAdapter(adapter);

        navView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_sensors_data:
                    viewPager.setCurrentItem(0, true);
                    break;
                case R.id.navigation_modules_state:
                    viewPager.setCurrentItem(1, true);
                    break;
                case R.id.navigation_tech_card:
                    viewPager.setCurrentItem(2, true);
                    break;
                case R.id.navigation_account:
                    viewPager.setCurrentItem(3, true);
                    break;
            }
            return true;
        });

        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        navView.setSelectedItemId(R.id.navigation_sensors_data);
                        break;
                    case 1:
                        navView.setSelectedItemId(R.id.navigation_modules_state);
                        break;
                    case 2:
                        navView.setSelectedItemId(R.id.navigation_tech_card);
                        break;
                    case 3:
                        navView.setSelectedItemId(R.id.navigation_account);
                        break;
                }
            }
        });

        Log.d("BackStack: ", String.valueOf(getSupportFragmentManager().getBackStackEntryCount()));

        navView.findViewById(R.id.navigation_sensors_data).setOnLongClickListener(view -> {

            showModalDialog();

            return false;
        });
    }

    private void showModalDialog() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        BoxesFragment getBoxesFragment = BoxesFragment.newInstance("Boxes");
        getBoxesFragment.show(fragmentManager, "fragment get boxes");
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();

        if (manager.getBackStackEntryCount() > 0) {
            manager.popBackStack();
            navView.setVisibility(View.VISIBLE);
        } else {
            finish();
        }
    }
}