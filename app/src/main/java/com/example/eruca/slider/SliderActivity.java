package com.example.eruca.slider;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.eruca.R;
import com.example.eruca.auth.LoginActivity;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

public class SliderActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Button mButton;

    private int[] layouts;
    private SliderAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_slider);

        Objects.requireNonNull(getSupportActionBar()).hide();

        viewPager = findViewById(R.id.view_pager);
        mButton = findViewById(R.id.slider_button);
        tabLayout = findViewById(R.id.tab_indicator);

        layouts = new int[]{
                R.layout.slider_fragment_1,
                R.layout.slider_fragment_2,
                R.layout.slider_fragment_3
        };

        adapter = new SliderAdapter(this, layouts);
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);

        mButton.setOnClickListener(view -> {
            if (viewPager.getCurrentItem() + 1 < layouts.length) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
            } else {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

        viewPager.addOnPageChangeListener(viewPagerChangeListener);
    }

    ViewPager.OnPageChangeListener viewPagerChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (position == layouts.length - 1) {
                mButton.setText(R.string.slider_button_end);
            } else {
                mButton.setText(R.string.slider_button_next);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}
