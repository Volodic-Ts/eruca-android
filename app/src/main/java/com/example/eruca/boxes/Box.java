package com.example.eruca.boxes;

public class Box {
    private String boxId;
    private String boxTitle;

    public Box(String boxId, String boxTitle) {
        this.boxId = boxId;
        this.boxTitle = boxTitle;
    }

    public String getBoxId() {
        return boxId;
    }

    public String getBoxTitle() {
        return boxTitle;
    }
}
