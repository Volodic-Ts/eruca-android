package com.example.eruca.boxes;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eruca.BuildConfig;
import com.example.eruca.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class BoxesFragment extends DialogFragment {

    private BoxViewModel boxViewModel;

    private RequestQueue mQueue;

    private ListView mListView;
    private JSONArray boxesArray;

    public BoxesFragment() {

    }

    public static BoxesFragment newInstance(String title) {
        BoxesFragment fragment = new BoxesFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boxViewModel = new ViewModelProvider(requireActivity()).get(BoxViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_get_boxes, container);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mQueue = Volley.newRequestQueue(getContext());

        String TOKEN = requireActivity().getIntent().getStringExtra("token");

        mListView = view.findViewById(R.id.boxes_list);

        getUserBoxesFromServer(TOKEN);

        mListView.setOnItemClickListener((adapterView, view1, i, l) -> {
            String itemValue = (String) mListView.getItemAtPosition(i);

            String boxId = itemValue.substring(4, 22);
            String boxTitle = itemValue.substring(30);

            Box box = new Box(boxId, boxTitle);
            boxViewModel.setBoxValues(box);

            dismiss();
//            Log.e("ID: ", boxId);
//            Log.e("TITLE: ", boxTitle);

        });

        mListView.requestFocus();
        Objects.requireNonNull(getDialog()).getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);
    }

    void getUserBoxesFromServer(String token) {
        String url = BuildConfig.SERVER_URL + "/boxes/get";

        ArrayList<String> arrayList = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, arrayList);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, response -> {
            try {
                boxesArray = response.getJSONArray("boxes");

                for (int i = 0; i < boxesArray.length(); i++) {
                    JSONObject box = boxesArray.getJSONObject(i);

                    String boxId = String.valueOf(box.getLong("id"));
                    String boxTitle = box.getString("title");

                    arrayList.add(getString(R.string.box_id) + " " + boxId + "\n" + getString(R.string.box_title) + " " + boxTitle);

                    mListView.setAdapter(adapter);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, Throwable::printStackTrace) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + token);

                return headerMap;
            }
        };

        mQueue.add(request);
    }
}
