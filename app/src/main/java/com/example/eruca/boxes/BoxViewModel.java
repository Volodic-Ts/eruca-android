package com.example.eruca.boxes;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BoxViewModel extends ViewModel {

    private MutableLiveData<Box> data;

    public void setBoxValues(Box box) {
        data.setValue(box);
    }

    public MutableLiveData<Box> getData() {
        if (data == null) {
            data = new MutableLiveData<>();
        }

        return data;
    }
}
