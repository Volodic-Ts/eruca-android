package com.example.eruca.splash;

import android.app.LauncherActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.eruca.MainActivity;
import com.example.eruca.R;
import com.example.eruca.auth.LoginActivity;
import com.example.eruca.room.AppExecutors;
import com.example.eruca.room.DatabaseController;
import com.example.eruca.room.database.UserDatabase;
import com.example.eruca.room.entity.UserTable;
import com.example.eruca.slider.SliderActivity;

import java.util.List;
import java.util.Objects;

public class Splash extends AppCompatActivity {

    private RequestQueue mQueue;
    private LauncherManager mLauncherManager;
    UserDatabase database;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        database = UserDatabase.getInstance(this);

        mQueue = Volley.newRequestQueue(this);

        DatabaseController controller = new DatabaseController(this);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        Objects.requireNonNull(getSupportActionBar()).hide();

        mQueue = Volley.newRequestQueue(this);

        mLauncherManager = new LauncherManager(this);

        SharedPreferences mSharedPreferences = getSharedPreferences("remember_me", MODE_PRIVATE);
        String flag = mSharedPreferences.getString("flag", "");


        new Handler().postDelayed(() -> {
            if (mLauncherManager.isFirstTime()) {
                mLauncherManager.setFirstLaunch(false);
                startActivity(new Intent(getApplicationContext(), SliderActivity.class));
            } else {
                if (flag.equals("true")) {
                    String login = mSharedPreferences.getString("login", "");
                    mLauncherManager.makeAuthRequest(login,
                            controller.getSavedUserDetailsByLogin(login), this, mQueue, this);
                } else {
                    startActivity(new Intent(this, LoginActivity.class));
                }
            }
        }, 1500);
    }
}
