package com.example.eruca.splash;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.eruca.BuildConfig;
import com.example.eruca.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class LauncherManager {

    //init shared preferences for checking about first and other launches
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private static String PREF_NAME = "LaunchManager";
    private static String IS_FIRST_TIME = "isFirst";

    public LauncherManager(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        editor = sharedPreferences.edit();
    }

    public void setFirstLaunch(boolean isFirst) {
        editor.putBoolean(IS_FIRST_TIME, isFirst);
        editor.commit();
    }

    public boolean isFirstTime() {
        return sharedPreferences.getBoolean(IS_FIRST_TIME, true);
    }

    public void makeAuthRequest(String login, String password, Context context, RequestQueue mQueue, Activity mActivity) {
        String url = BuildConfig.SERVER_URL + "/auth";

        JSONObject postData = new JSONObject();

        try {
            postData.put("username", login);
            postData.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, postData, response -> {
            try {
                String TOKEN = response.getString("jwt");

                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra("token", TOKEN);
                intent.putExtra("login", login);

                mActivity.startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> {
            Log.e("Error: ", postData.toString());
            error.printStackTrace();
        });

        mQueue.add(request);
    }
}

